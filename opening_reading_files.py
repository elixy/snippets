#!/home/tofu/Developer/Projects/python_automation/venv/bin/python3.8

f = open('a.txt', 'r')
content = f.read(5)
print(content)
print(f.closed)
f.close()
print(f.closed, end='\n')


print('-----' * 3, 'seperation', '-----' *3, end='\n')

with open('a.txt', 'r') as file:
    print(file.read())
print('\n')
print(file.closed)


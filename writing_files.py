#!/home/tofu/Developer/Projects/python_automation/venv/bin/python3.8

with open('myfile.txt', 'a') as f:    #  w for write, r for read, a for append,  r+ for read and write
    f.write("abc\n")
    f.write('Just a 2nd line\n')

with open('configuration.txt', 'r+') as f:
    f.seek(5)
    f.write('100')

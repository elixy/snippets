import pickle

friends = {"Dan": [20, "london", 41435768], "Marie": [23, "Spain", 2453456543], "Mike": [21, "U.K.", 23454345]}

with open('friends.dat', 'wb') as f:
    pickle.dump(friends, f)

with open('friends.dat', 'rb') as f:
    obj = pickle.load(f)
    print(type(obj))
    print(obj)

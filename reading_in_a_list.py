#!/home/tofu/Developer/Projects/python_automation/venv/bin/python3.8

with open('configuration.txt') as file:
    my_list = file.read().splitlines()
    print(my_list)

with open('configuration.txt', 'r') as file:
    my_list = file.readlines()
    print(my_list)

with open('configuration.txt', 'r') as file:
    print(file.readline())
    print(file.readline())

print('-----' * 3, 'seperation', '-----' *3)


with open('configuration.txt', 'r') as file:
    for line in file:
        print(line)

print('-----' * 3, 'seperation', '-----' *3)

with open('configuration.txt', 'r') as f:
    print(f.read())
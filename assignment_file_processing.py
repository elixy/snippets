import csv

with open('devices.txt') as f:
    reader = csv.reader(f, delimiter=':')
    final_list = []
    for row in reader:
        final_list.append(row)

    print(final_list)

# ##### Second answer
#
#
# with open('devices.txt') as f:
#     devices = f.read().splitlines()
#     print(devices, end='\n\n')
#
# mylist = list()
# for item in devices:
#     tmp = item.split(':')
#     mylist.append(tmp)
#
#
# print(mylist)
